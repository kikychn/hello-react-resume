import React, { Component } from 'react';
import './App.less';

class App extends Component {
  render() {
    return <main className="app">start here</main>;
  }
}

export default App;
